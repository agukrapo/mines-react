import React, { Component } from 'react';
import './Game.css';

class Point {
  constructor(i, j) {
    this.i = i;
    this.j = j;
  }

  equals(otherPoint) {
    return this.i === otherPoint.i && this.j === otherPoint.j;
  }

  valueIn(squares) {
    return squares[this.i][this.j];
  }

  neighbors(maxIvalue, maxJvalue) {
    let result = [];
    for (const i of this.values(this.i, maxIvalue - 1)) {
      for (const j of this.values(this.j, maxJvalue - 1)) {
        const newPoint = new Point(i, j);
        if (!this.equals(newPoint)) {
          result.push(newPoint);
        }
      }
    }
    return result;
  }

  values(curr, max) {
    let result = [];
    result.push(curr);
    if (curr > 0) result.push(curr - 1);
    if (curr < max) result.push(curr + 1);
    return result;
  }
}

class Square extends Component {
  translateValue(value) {
    switch(value) {
      case 'NEW':
        return '';
      case 'HIDDEN_MINE':
        return '*';
      case 'MINE':
        return 'O';
      case 'EXPLODED_MINE':
        return 'X';
      case 'SAFE':
        return '·';
      default:
        return value;
    }
  }

  render() {
    return (
      <button className="square" onClick={this.props.onClick}>
        {this.translateValue(this.props.value)}
      </button>
    );
  }
}

class Board extends Component {
  renderSquare(i, j) {
    return (
      <Square
        value={this.props.squares[i][j]}
        onClick={() => this.props.onClick(i, j)}
      />
    );
  }

  render() {
    const rows = this.props.squares.map((row, i) => {
      const rowSquares = row.map((square, j) => {
        return(
          <div key={'' + i + j}>
            {this.renderSquare(i, j)}
          </div>
        );
      });

      return (
        <div key={i} className='row'>
          {rowSquares}
        </div>
      );
    });

    return (
      <div className='board'>
        {rows}
      </div>
    );
  }
}

class ConfigForm extends Component {
  constructor(props) {
    super(props);

    this.state = {difficulty: 'MEDIUM'};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({difficulty: event.target.value});
  }

  handleSubmit(event) {
    this.props.onNewGame(this.state.difficulty);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Difficulty:
          <select value={this.state.difficulty} onChange={this.handleChange}>
            <option value="EASY">Easy</option>
            <option value="MEDIUM">Medium</option>
            <option value="HARD">Hard</option>
          </select>
        </label>
        <input type="submit" value="New game" />
      </form>
    );
  }
}

class Game extends Component {
  difficulties = {
    'EASY': {
      rows: 8,
      columns: 10,
      mines: 10
    },
    'MEDIUM': {
      rows: 13,
      columns: 16,
      mines: 40
    },
    'HARD': {
      rows: 18,
      columns: 26,
      mines: 99
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      gameOver: false,
      firstClick: true,
      minesLocation: [],
      squares: [],
      difficulty: null
    }
  }

  placeMines(squares, locations, value) {
    for (let mine of locations) {
      squares[mine[0]][mine[1]] = value;
    }
  }

  randomMinesLocation(i, j) { //FIXME make unique
    const result = [];
    while (result.length < this.state.difficulty.mines) {
      let mine = [
        this.randomPosition(i, this.state.difficulty.rows), 
        this.randomPosition(j, this.state.difficulty.columns)
      ];
      result.push(mine);
    }
    return result;
  }

  randomPosition(pos, maxValue) {
    return Math.floor(Math.random() * maxValue);    
  }

  handleClick(i, j) {
    if (!this.state.gameOver) {
      const squareValue = this.state.squares[i][j];
      const clonedSquares = this.state.squares.slice();

      if (this.state.firstClick) {
        const minesLocation = this.randomMinesLocation(i, j);
        this.setState({firstClick: false, minesLocation: minesLocation});
        this.placeMines(clonedSquares, minesLocation, 'HIDDEN_MINE');
      } 

      if (squareValue === 'NEW') {
        this.discoverAdjancentSquares(new Point(i, j), clonedSquares);

      } else if (squareValue === 'HIDDEN_MINE') {
        this.placeMines(clonedSquares, this.state.minesLocation, 'MINE');
        clonedSquares[i][j] = 'EXPLODED_MINE';
        this.setState({gameOver: true});
      }

      this.setState({squares: clonedSquares});
    }
  }

  discoverAdjancentSquares(point, squares) {
    squares[point.i][point.j] = 'SAFE';
    
    let minesCounter = 0;
    let safeNeighbors = [];
    for (let neighbor of point.neighbors(this.state.difficulty.rows, this.state.difficulty.columns)) {
      if (neighbor.valueIn(squares) === 'HIDDEN_MINE') {
        minesCounter++;
      } else if (neighbor.valueIn(squares) === 'NEW') {
        safeNeighbors.push(neighbor);
      }
    }    

    if (minesCounter > 0) {
      squares[point.i][point.j] = '' + minesCounter;
    } else {
      for (let neighbor of safeNeighbors) {
        this.discoverAdjancentSquares(neighbor, squares);
      }
    }
  }

  handleNewGame(value) {
    const difficulty = this.difficulties[value];
    const squares = Array(difficulty.rows).fill(null).map(x => Array(difficulty.columns).fill('NEW'));

    this.setState({
      gameOver: false,
      firstClick: true,
      squares: squares, 
      difficulty: difficulty
    });
  }

  render() {
    return (
      <div>
        <h1>Mines</h1>
        <ConfigForm onNewGame={difficulty => this.handleNewGame(difficulty)} />
        <Board squares={this.state.squares} onClick={(i, j) => this.handleClick(i, j)} />
      </div>
    );
  }
}

export default Game;
